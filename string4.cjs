// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


function string4(obj){

    let fullName = '';

    if (!(obj === undefined) && typeof obj === 'object' && !Array.isArray(obj)) {

        if (obj.first_name && obj.last_name && typeof obj.first_name == 'string' && typeof obj.last_name === 'string') {

            firstName = toTitleCase(obj.first_name.trim());
            lastName = toTitleCase(obj.last_name.trim());
 
            if (obj.middle_name && typeof obj.middle_name === 'string') {
                middleName= toTitleCase(obj.middle_name.trim());
                fullName= firstName+' '+middleName+' '+lastName;
            }
            else{
                fullName= firstName+' '+lastName;
            }
            }
        
    }

    return fullName;
};

function toTitleCase(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
}


module.exports = string4;
