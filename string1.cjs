// function to convert the strings of format "$100.45", "$1,002.22", "-$123", etc into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. 
// If the number is invalid, return 0


function string1(str){

    let number = 0;
    if (!(str === undefined) && typeof str === 'string') {
        str = str.trim();

        if (str.charAt(0) === '$') {
            str = str.slice(1);
        } 
        else if (str.startsWith('-$') || str.startsWith('+$')) {
            str = str.substring(0,1) + str.substring(2,)
        }
        str = str.replace(/,/g, '');
        if (!isNaN(Number(str))) {
            number = Number(str);
        }
    }
    return number;
}

module.exports = string1;
