// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.


function string5(arr){

    let result = '';

    if (!(arr === undefined) && Array.isArray(arr)) {

        for (let i = 0; i < arr.length; i++) {
            let word = arr[i]
            if (typeof word === 'string') {
                result = result.concat(' '+word.trim());
            }
        }
    }

    return result;
};


module.exports = string5;
