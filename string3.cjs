// Given a string in the format of "10/1/2021", 
// function print the month in which the date is present in.

function string3(str){

    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December'];
    
    const monthOf31 = [1,3,5,7,8,10,12]
    const monthOf30 = [4,6,9,11]

    const RegEx = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

    if (str && typeof str == 'string') {
        str = str.trim();

        if (str.match(RegEx)) {
            let [day, month, year] = str.split('/');
            day = parseInt(day);
            month = parseInt(month);

            if ((monthOf31.includes(month) && day > 0 && day <= 31) || (monthOf30.includes(month) && day > 0 && day <= 30) || (month === 2 && day > 0 && day <= 29)){
                result = monthNames[month - 1];
                console.log(result);
            }
        }
    }
}


module.exports = string3;
