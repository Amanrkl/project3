// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.



function string2(str){

    let ipv4Components = [];
    
    if (str && typeof str === 'string') {
        str = str.trim();
        str = str.split('.');

        if (str.length == 4){

            for (let i = 0; i < str.length; i++) {
                let component = str[i]
                let numComponent = parseInt(component)
                if ( numComponent.toString() === component && numComponent>=0 && numComponent <=255 ){
                    ipv4Components.push(numComponent)
                }

            }
        }
    }
    if (ipv4Components.length == 4){
        return ipv4Components
    }
    else{
        return []
    }


}

module.exports = string2;
