const string1 = require('../string1.cjs')

console.log(string1("$100.45"));
console.log(string1("$1,002.22"));
console.log(string1("--$123"));
console.log(string1("-$123.0012"));
console.log(string1("$1,0,,02.22"));
console.log(string1(123));
console.log(string1(1778,823));
console.log(string1([123]));
console.log(string1({123:null}));